import argparse, os, logging, sys, json, time
from plugin import Checker
import threading
from flask import Flask, render_template_string, redirect, request, send_file, make_response
from flask_sock import Sock
num_file_upload = 0


def runws(ch:Checker, out, data):
    app = Flask("Checker")
    sock = Sock(app)
    d = {topic:[{"i":i + 1, "res":"", "err":""} for i in range(len(ch.topics[topic]["tests"]))] for topic in ch.topics.keys()}
    o = {topic:[] for topic in ch.topics.keys()}
    @sock.route("/ws")
    def testsock(ws):
        while True:
            for topic in d.keys():
                try:
                    ws.send(json.dumps({"topic":topic, "array":d[topic], "complerr":None if d[topic][0]["res"] != "CE" else d[topic][0]["err"].replace("\n", "<br>").replace(" ","&nbsp;")})) # .replace("<", "&lt;").replace(">", "&gt;").replace("\n", "<br>")
                except:
                    pass
            time.sleep(1)
    def runtest(args, filenames, topic):
        res = ch.run(*args)
        rmfl = True
        if res[0]["error"]:
            if res[0]["error"][0] == "CE":
                rmfl = False
        if rmfl:
            for filename in filenames:
                os.remove(filename)
        o[topic].append(res)
        if out.out:
            with open(out.out, "w") as f:
                f.write(json.dumps(o))
    def run_task(test, res, i, lastdata, topic):
        if res["is_true"]:
            d[topic][i - 1]["res"] = "OK"
        else:
            if res["error"]:
                d[topic][i - 1]["res"] = res["error"][0]
                if res["error"][0] == "CE":
                    d[topic][i - 1]["err"] = res["error"][1]
            else:
                d[topic][i - 1]["res"] = "WA"
            

    @app.route("/")
    def index():
        return render_template_string("""
<!DOCTYPE html>
<html>
<head>
  <title>Checker</title>
  <style>
.container {
  display: flex;
  height: 100vh; /* set the height of the container to 100% of the viewport height */
  border: 2px solid black;
}

.menu {
  width: 200px;
  background-color: #f0f0f0;
  height: 100%; /* set the height of the menu to 100% of the container */
}

.content {
  flex-grow: 1;
  padding: 20px;
}
  </style>
</head>
<body>
  <div class="container">
    <div class="menu">
      <!-- Menu items go here -->
      <ul>
        <li><a href="/">WebServer</a></li>
        <ul>
            <li><a href="/">Tasks</a></li>
            <ul>
                {% for task in tasks %}
                    <li><a href="/task/{{ task.path }}">{{ task.name }}</a></li>
                {% endfor %}
            </ul>
        </ul>
      </ul>
    </div>
    <div class="content">
      <!-- Content goes here -->
      <h1>Choice Topic:</h1>
      <ul>
        {% for task in tasks %}
            <li><a href="/task/{{ task.path }}">{{ task.name }}</a></li>
        {% endfor %}
      </ul>
    </div>
  </div>
</body>
</html>
                """, tasks=[{"path":task.replace(" ", "_"), "name":task} for task in ch.topics.keys()])

    @app.route("/task/<task>", methods=["GET", "POST"])
    def task(task):
        global num_file_upload
        task_name = task
        task_ = None
        for topic in ch.topics.keys():
            if topic.replace(" ", "") == task_name:
                task_ = topic
        if task_:
            if request.method == "POST":
                txt = None
                if request.files["file"]:
                    file = request.files["file"]
                    if file.filename.split(".")[-1] == data["upload_extension"]:
                        name = "program" + str(num_file_upload)
                        with open("program" + str(num_file_upload) + "." + data["upload_extension"], "wb") as f:
                            num_file_upload += 1
                            f.write(file.read())
                        d[task_] = [{"i":i + 1, "res":"Waiting...", "err":""} for i in range(len(ch.topics[task_]["tests"]))]
                        cmp = str(out.comp)
                        try:
                            cmp = cmp%(name, name)
                        except:
                            pass
                        rmfiles = [name+"."+data["upload_extension"]]
                        for i in data["remove_also_files"]:
                            rmfiles.append(i%name)
                        args = []
                        ck = out.check%name
                        args.append([ck, task_, cmp, (lambda test, res, i, lastdata, topic=task_:run_task(test, res, i, lastdata, topic))])
                        args.append(rmfiles)
                        args.append(str(task_))
                        threading.Thread(target=runtest, args=args).start()
                    else:
                        txt = "Invalid Extension"
                else:
                    txt = "No file attached"
                if txt:
                    return render_template_string("""
<!DOCTYPE html>
<html>
<head>
  <title>Checker</title>
  <style>
.container {
  display: flex;
  height: 100vh; /* set the height of the container to 100% of the viewport height */
  border: 2px solid black;
}

.menu {
  width: 200px;
  background-color: #f0f0f0;
  height: 100%; /* set the height of the menu to 100% of the container */
}

.content {
  flex-grow: 1;
  padding: 20px;
}
  </style>
</head>
<body>
  <div class="container">
    <div class="menu">
      <!-- Menu items go here -->
      <ul>
        <li><a href="/">WebServer</a></li>
        <ul>
            <li><a href="/">Tasks</a></li>
            <ul>
                {% for task in tasks %}
                    <li><a href="/task/{{ task.path }}">{{ task.name }}</a></li>
                {% endfor %}
            </ul>
        </ul>
      </ul>
    </div>
    <div class="content">
      <!-- Content goes here -->
      <h1>{{ text }}</h1>
    </div>
  </div>
</body>
</html>
                                                """, tasks=[{"path":tsk.replace(" ", "_"), "name":tsk} for tsk in ch.topics.keys()],
                                                text=txt)
            return render_template_string("""
<!DOCTYPE html>
<html>
<head>
  <title>Checker</title>
  {% if test %}
    <script>
      const ws = new WebSocket('ws://' + window.location.host + "/ws")
      var lastdata = null;
      function descriptionError(error){
        switch (error){
          case "WA": return "Wrong Answer"
          case "CE": return "Compilation Error"
          case "OK": return "Test passed"
          case "TL": return "Time Limit"
          case "ML": return "Memory Limit"
          case "RE": return "Runtime error"
          case "FT": return "Failed to Test"
          default: return "Waiting..."
        }
      }
      function getPoints(){
        if (lastdata != null){
          let len = lastdata.array.length;
          let maded = 0;
          lastdata["array"].forEach(element => {
              if (element.res == "OK"){
                maded++;
              };
          });
          return parseInt(100*(maded/len));
        }
      }
      ws.onmessage = function(e){
          let data = JSON.parse(e.data)
          if (data.topic=="{{ topic }}"){
              lastdata = data;
              data["array"].forEach(element => {
                  document.getElementById("inp"+element.i).innerHTML = element.res;
                  document.getElementById("err"+element.i).innerHTML = descriptionError(element.res);
              });
              if (data.complerr != null){
                document.getElementById("complerrh1").style.display = "block";
                document.getElementById("complerr").innerHTML = data.complerr;
              }
              let points = getPoints();
              if (points != null){
                document.getElementById("titlename").innerHTML = "Task: " + data.topic + " (" + points.toString() + "/100)";
              }
          }
      }
      ws.onopen = function(e) {
        console.log("Connected...")
      }
    </script>
    {% endif %}
  <style>
.container {
  display: flex;
  height: 100vh; /* set the height of the container to 100% of the viewport height */
  border: 2px solid black;
}

.menu {
  width: 200px;
  background-color: #f0f0f0;
  height: 100%; /* set the height of the menu to 100% of the container */
}

.content {
  flex-grow: 1;
  padding: 20px;
}
.upload{
    border: 2px solid black;
    background-color: rgb(7, 168, 168);
}
th, td{
    border: 2px solid black;
    padding: 8px;
}
table {
    border-collapse: collapse;
    width: 100%;
}
  </style>
</head>
<body>
  <div class="container">
    <div class="menu" style="overflow: scroll;">
      <!-- Menu items go here -->
      <ul>
        <li><a href="/">WebServer</a></li>
        <ul>
            <li><a href="/">Tasks</a></li>
            <ul>
                {% for task in tasks %}
                    <li><a href="/task/{{ task.path }}">{{ task.name }}</a></li>
                {% endfor %}
            </ul>
        </ul>
      </ul>
    </div>
    <div class="content" style="overflow:scroll;">
      <!-- Content goes here -->
      <h1 id="titlename">Task: {{ topic }}</h1>
      <table style="width: 500px;">
        <tbody>
          <tr>
            <th>Time:</th>
            <th>Memory:</th>
          </tr>
          <tr>
            <td>{{ time }} s</td>
            <td>{{ memory }} MB</td>
          </tr>
        </tbody>
      </table>
      {% if pdf %}
        <div style="padding: 10px;"></div>
        <h2><a href="/task/pdf/{{ topic }}">View PDF</a></h2>
      {% endif %}
      <div style="padding: 10px;"></div>
      <div class="upload">
        <h2 style="color:white">Upload</h2>
        <form method="POST" action="/task/{{ topic }}" enctype="multipart/form-data">
            <input type="file" name="file">
            <div></div>
            <input type="submit" value="Upload file" style="background-color: greenyellow; margin-top: 10px;">
        </form>
      </div>
      <div style="padding: 10px;"></div>
      <div>
        {% if test %}
            <table>
                <tr>
                    <th>Test</td>
                    <th>Result</td>
                    <th>Error Description</td>
                </tr>
                {% for tst in test.tests %}
                    <tr>
                        <td>Test {{ tst.i }}</th>
                        <td id="inp{{ tst.i }}">{{ tst.res }}</th>
                        <td id="err{{ tst.i }}">{{ tst.err }}</th>
                    </tr>
                {% endfor %}
            </table>
            <h1 id="complerrh1" style="display: none;">Compilation Error</h1><br>
            {% if test.complerr %}
            <code id="complerr">{{ test.complerr }}</code>
            {% else %}
            <code id="complerr"></code>
            {% endif %}
        {% endif %}
      </div>
      </ul>
    </div>
  </div>
</body>
</html>
                                          """, tasks=[{"path":tsk.replace(" ", "_"), "name":tsk} for tsk in ch.topics.keys()],
                                          topic=task_,
                                          test= {"tests":d[task_], "complerr":None if d[task_][0]["res"] != "CE" else d[task_][0]["err"].replace("<", "&lt;").replace(">", "&gt;").replace("\n", "<br>")} if d[task_][0]["res"] != "" else None,
                                          time=ch.topics[task_]["limits"]["time"],
                                          memory=ch.topics[task_]["limits"]["memory"],
                                          pdf=True if ch.topics[task_]["pdf"] else False,
                                          tree=data["webserver_tree"]
                                          )
        else:
            return redirect("/")
    @app.route("/task/pdf/<task_name>")
    def task_pdf(task_name):
        task_ = None
        for tk in ch.topics.keys():
            if tk == task_name:
                task_ = task_name
                break
        if task_:
            if ch.topics[task_]["pdf"]:
                pdf = send_file(ch.topics[task_]["pdf"])
                response = make_response(pdf)
                response.headers["Content-Type"] = "application/pdf"
                return response
            else:
                return redirect("/task/"+task_name)
        else:
            return redirect("/")
      
    app.run(host=data["host"], port=data["port"], debug=data["debug"])


if __name__ == "__main__":
    applog = logging
    arg = argparse.ArgumentParser(prog="Checker Arena")
    arg.add_argument("-f", "--folder", help="choice tasks folder")
    arg.add_argument("-j", "--cout", help="compile output")
    arg.add_argument("-t", "--tin", help="json file of task to load")
    arg.add_argument("-o", "--out", help="output file")
    arg.add_argument("-c", "--check", help="check file")
    arg.add_argument("-p", "--topic", help="topic name")
    arg.add_argument("-g", "--comp", help="compile command")
    arg.add_argument("--ws", help="run webserver")
    out = arg.parse_args()
    ch = Checker()
    print("Running...")
    if out.folder:
        if os.path.isdir(out.folder):
            applog.info("Reading folder...")
            ch.load_tasks_dir(out.folder)
        else:
            applog.error("Invalid folder")
            sys.exit(1)
        if out.cout:
            applog.info("Saving file")
            with open(out.cout, "w") as f:
                json.dump(ch.topics, f)
    elif out.tin:
        if os.path.isfile(out.tin):
            applog.info("Reading file...")
            with open(out.tin,) as f:
                ch.topics = json.load(f)
        else:
            applog.error("Invalid file")
            sys.exit(1)
    if out.check and out.topic:

        def test(test, res, i, lastdata):
            print("Test", i, end=" ")
            if res["is_true"]:
                print("OK")
                return
            if res["error"] == None:
                print("WA")
                return
            print(res["error"][0])

        applog.info("Checking...")
        o = ch.run(cmd=out.check, topic=out.topic, compile_cmd=out.comp, on_test=test)
        if out.out:
            applog.info("Saving...")
            with open(out.out, "w") as f:
                json.dump(o, f)
    if out.ws:
        try:
            data = json.load(open(out.ws, "rb"))
            runws(ch, out, data)
        except:
            print("Failed to load ws (%s)" % out.ws)
