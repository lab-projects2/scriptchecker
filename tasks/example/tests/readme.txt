here you create the test like this:
"{task_name}.{index}.{type}"

task_name - the app thinks that the folder name of the topic is the task name
index - the index of the input
type - "in" - means that is the input of the test; "sol" - means that is the input of the test

examples:
test.01.in
test.01.sol
test.02.in
test.02.sol