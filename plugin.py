import json, os, re, psutil
import subprocess
import time
from concurrent.futures import ThreadPoolExecutor, wait
from jproperties import Properties

class Checker:
    def __init__(self) -> None:
        self.topics = {}
        self.tests_regex = "\w+.\d+.\w+"
        self.tests_get_name = lambda x: x.split(".")[0]
        self.tests_get_num = lambda x: int(x.split(".")[1].removeprefix("0"))
        self.tests_is_input = lambda x: True if x.split(".")[2] == "in" else False 
        self.limits_default_time = 0.1
        self.limits_default_memory = 256
        self.data_author_file_regex = "author.\w+"
    def load_tasks_dir(self, path):
        for folder in os.listdir(path):
            if os.path.isdir(os.path.join(path, folder)):
                dir_path = os.path.join(path, folder)
                topic_name = os.path.basename(os.path.abspath(dir_path))
                self.topics[topic_name] = {"tests":[], "limits":{"time":self.limits_default_time, "memory":self.limits_default_memory, "best_time":None, "best_time_add":0.1}, "author":None, "pdf":None}
                if os.path.isdir(os.path.join(dir_path, "tests")):
                    tests = {}
                    for (dirpath, dirname, dirfile) in os.walk(os.path.join(dir_path, "tests")):
                        for file in dirfile:
                            if re.match(self.tests_regex,file):
                                with open(os.path.join(dirpath, file)) as f:
                                    name = self.tests_get_name(file)
                                    num = int(self.tests_get_num(file))
                                    isinput = bool(self.tests_is_input(file))
                                    snum = str(num)
                                    if name != topic_name: raise ValueError("Topic %s doesn't match test file name %s (file %s, name %s)" % (topic_name.__repr__(), name.__repr__(), os.path.join(dirpath, file), file))
                                    if not snum in tests.keys():
                                        tests[snum] = {}
                                    if not "in" in tests[snum].keys():
                                        if isinput:
                                            tests[snum]["in"] = f.read().lstrip("\n").rstrip("\n")+"\n"
                                    if not "out" in tests[snum].keys():
                                        if not isinput:
                                            tests[snum]["out"] = f.read().lstrip("\n").rstrip("\n")
                    i = 0
                    while True:
                        si = str(i)
                        if i == 0:
                            if not si in tests.keys():
                                i+=1
                                continue
                        elif not si in tests.keys():
                            break
                        if ("in" in tests[si].keys()) and ("out" in tests[si].keys()):
                            self.topics[topic_name]["tests"].append(tests[si])
                        i+=1
                if os.path.isfile(os.path.join(dir_path, "grade.properties")):
                    limits = Properties()
                    with open(os.path.join(dir_path, "grade.properties"), 'rb') as config_file:
                        limits.load(config_file)
                    if limits.get("memory"):
                        self.topics[topic_name]["limits"]["memory"] = float(limits.get("memory").data)
                    if limits.get("time"):
                        self.topics[topic_name]["limits"]["time"] = float(limits.get("time").data)
                if os.path.isfile(os.path.join(dir_path, topic_name+".pdf")):
                    self.topics[topic_name]["pdf"] = os.path.join(dir_path, topic_name+".pdf")
                for (dirpath, dirfolder, dirfiles) in os.walk(os.path.join(dir_path, "author")):
                    for file in dirfiles:
                        if re.match(self.data_author_file_regex, file):
                            self.topics[topic_name]["author"] = os.path.join(dirpath, file)
    def run(self, cmd, topic:str,compile_cmd:str=None, on_test=lambda test, resault, i, lastdata: None):
        assert topic in self.topics.keys()
        results = []
        if compile_cmd:
            o = subprocess.run(compile_cmd, capture_output=True, shell=True)
            if o.returncode != 0:
                for i in range(1, len(self.topics[topic]["tests"])+1):
                    on_test(self.topics[topic]["tests"][i-1], {"is_true":False, "error":["CE", o.stderr.decode()], "got":""}, i, [{"is_true":False, "error":["CE", o.stderr.decode()], "got":""}]*i)
                return [{"is_true":False, "error":["CE", o.stderr.decode()], "got":""}]*len(self.topics[topic]["tests"])

        def check(mem, tl, p):
            reson = None
            try:
                t = time.time()
                while True:
                    proc = psutil.Process(p.pid)
                    if proc.memory_info()[0] / float(2 ** 20) > mem:
                        reson = ["ML", proc.memory_info()[0] / float(2 ** 20)]
                        proc.kill()
                    if round(time.time()-t, 3) > tl:
                        reson = ["TL", time.time()-t]
                        proc.kill()
            except:
                pass
            return reson
        i = 1
        for test in self.topics[topic]["tests"]:
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
            pool = ThreadPoolExecutor(max_workers=2)
            task = pool.submit(p.communicate, test["in"].encode())
            checker = pool.submit(check, self.topics[topic]["limits"]["memory"], self.topics[topic]["limits"]["time"], p)
            wait([task])
            stdout, stderr = task.result()
            res = {"is_true":False, "got":"", "error":["FT"]}
            chres = checker.result()
            if stderr:
                res = {"is_true":False, "got":stdout.decode(), "error":["RE", chres, stderr.decode()]}
            else:
                if chres:
                    res = {"is_true":False, "got":stdout.decode(), "error":chres}
                else:
                    if test["out"] in stdout.decode():
                        res = {"is_true":True, "got":stdout.decode(), "error":None}
                    else:
                        res = {"is_true":False, "got":stdout.decode(), "error":None}
            results.append(res)
            on_test(test, res, i, results)
            i+=1
        return results
    def get_length_of_tests(self, topic:str):
        return len(self.topics[topic]["tests"])
